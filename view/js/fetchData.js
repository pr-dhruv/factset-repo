function getData() {
	document.getElementById("loader-view").style.display = "block";
	let selected = document.getElementById("patient-select").value;

	if(selected === "1" ||selected === "2" || selected === "3" || selected === "4" ) {
		var xhttp = new XMLHttpRequest();
		xhttp.open("GET","https://jsonmock.hackerrank.com/api/medical_records?userId="+selected,false);
		xhttp.send(null);

		var object = JSON.parse(xhttp.responseText);
		document.getElementById("loader-view").style.display = "none";
		putValues(object.data);
	}

}

function putValues(dataArr) {
	document.getElementById("profile-view").style.display = "block";
	document.getElementById("patient-name").innerHTML = dataArr[0].userName;
	document.getElementById("patient-dob").innerHTML = dataArr[0].userDob;
	document.getElementById("patient-height").innerHTML = "Height: "+dataArr[0].meta.height;

	var headRow = "<th>SL</th><th>Date</th><th>Diagnosis</th><th>Doctor</th><th>Weight</th>";
	var tableData = "";

	for(var a=0; a<dataArr.length; a++){
		var SL = dataArr[a].id;
		var date = new Date(dataArr[a].timestamp);
		var dateStr = ("0" + date.getDate()).slice(-2)+"/"+("0" + date.getMonth()).slice(-2)+"/"+date.getFullYear();

		var diagnosis = dataArr[a].diagnosis.name;
		var dgId = dataArr[a].diagnosis.id;
		var doctor = dataArr[a].doctor.name;
		var weight = dataArr[a].meta.weight;
		tableData = tableData + "<tr><td>"+SL+"</td><td>"+dateStr+"</td><td>"+diagnosis+"("+dgId+")"+"</td><td>"+doctor+"</td><td>"+weight+"</td></tr>";
	}
	document.getElementById("table-header").innerHTML = headRow;
	document.getElementById("table-body").innerHTML = tableData;
}

function getRecords() {
	debugger;
	document.getElementById("profile-view").style.display = "none";
	getData();
}